﻿namespace ESExplorer
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Main));
            this.mainToolStrip = new System.Windows.Forms.ToolStrip();
            this.toolStripDropDownButton1 = new System.Windows.Forms.ToolStripDropDownButton();
            this.fileExitButton = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripDropDownButton2 = new System.Windows.Forms.ToolStripDropDownButton();
            this.helpAboutButton = new System.Windows.Forms.ToolStripMenuItem();
            this.mainContainer = new System.Windows.Forms.SplitContainer();
            this.deleteServerButton = new System.Windows.Forms.Button();
            this.editServerButton = new System.Windows.Forms.Button();
            this.addServerButton = new System.Windows.Forms.Button();
            this.treeView = new System.Windows.Forms.TreeView();
            this.serverContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.foreachDocumentToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteFieldToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.queryGroupBox = new System.Windows.Forms.GroupBox();
            this.queryTextBox = new System.Windows.Forms.TextBox();
            this.verifyAndFormatButton = new System.Windows.Forms.Button();
            this.apiMethodComboBox = new System.Windows.Forms.ComboBox();
            this.resultGroupBox = new System.Windows.Forms.GroupBox();
            this.resultTextBox = new System.Windows.Forms.TextBox();
            this.toolTipButtons = new System.Windows.Forms.ToolTip(this.components);
            this.endpointGroupBox = new System.Windows.Forms.GroupBox();
            this.methodComboBox = new System.Windows.Forms.ComboBox();
            this.apiEndpointTextBox = new System.Windows.Forms.TextBox();
            this.runButton = new System.Windows.Forms.Button();
            this.mainToolStrip.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mainContainer)).BeginInit();
            this.mainContainer.Panel1.SuspendLayout();
            this.mainContainer.Panel2.SuspendLayout();
            this.mainContainer.SuspendLayout();
            this.serverContextMenuStrip.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.queryGroupBox.SuspendLayout();
            this.resultGroupBox.SuspendLayout();
            this.endpointGroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // mainToolStrip
            // 
            this.mainToolStrip.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.mainToolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripDropDownButton1,
            this.toolStripDropDownButton2});
            this.mainToolStrip.Location = new System.Drawing.Point(0, 0);
            this.mainToolStrip.Name = "mainToolStrip";
            this.mainToolStrip.Size = new System.Drawing.Size(1112, 27);
            this.mainToolStrip.TabIndex = 3;
            this.mainToolStrip.Text = "toolStrip1";
            // 
            // toolStripDropDownButton1
            // 
            this.toolStripDropDownButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripDropDownButton1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileExitButton});
            this.toolStripDropDownButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripDropDownButton1.Image")));
            this.toolStripDropDownButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripDropDownButton1.Name = "toolStripDropDownButton1";
            this.toolStripDropDownButton1.Size = new System.Drawing.Size(46, 24);
            this.toolStripDropDownButton1.Text = "File";
            // 
            // fileExitButton
            // 
            this.fileExitButton.Name = "fileExitButton";
            this.fileExitButton.Size = new System.Drawing.Size(108, 26);
            this.fileExitButton.Text = "Exit";
            this.fileExitButton.Click += new System.EventHandler(this.fileExitButton_Click);
            // 
            // toolStripDropDownButton2
            // 
            this.toolStripDropDownButton2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripDropDownButton2.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.helpAboutButton});
            this.toolStripDropDownButton2.Image = ((System.Drawing.Image)(resources.GetObject("toolStripDropDownButton2.Image")));
            this.toolStripDropDownButton2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripDropDownButton2.Name = "toolStripDropDownButton2";
            this.toolStripDropDownButton2.Size = new System.Drawing.Size(55, 24);
            this.toolStripDropDownButton2.Text = "Help";
            // 
            // helpAboutButton
            // 
            this.helpAboutButton.Name = "helpAboutButton";
            this.helpAboutButton.Size = new System.Drawing.Size(125, 26);
            this.helpAboutButton.Text = "About";
            this.helpAboutButton.Click += new System.EventHandler(this.helpAboutButton_Click);
            // 
            // mainContainer
            // 
            this.mainContainer.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.mainContainer.Location = new System.Drawing.Point(12, 30);
            this.mainContainer.Name = "mainContainer";
            // 
            // mainContainer.Panel1
            // 
            this.mainContainer.Panel1.Controls.Add(this.deleteServerButton);
            this.mainContainer.Panel1.Controls.Add(this.editServerButton);
            this.mainContainer.Panel1.Controls.Add(this.addServerButton);
            this.mainContainer.Panel1.Controls.Add(this.treeView);
            this.mainContainer.Panel1MinSize = 270;
            // 
            // mainContainer.Panel2
            // 
            this.mainContainer.Panel2.Controls.Add(this.endpointGroupBox);
            this.mainContainer.Panel2.Controls.Add(this.splitContainer1);
            this.mainContainer.Panel2MinSize = 520;
            this.mainContainer.Size = new System.Drawing.Size(1088, 535);
            this.mainContainer.SplitterDistance = 270;
            this.mainContainer.SplitterWidth = 10;
            this.mainContainer.TabIndex = 4;
            // 
            // deleteServerButton
            // 
            this.deleteServerButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.deleteServerButton.Enabled = false;
            this.deleteServerButton.Image = global::ESExplorer.Properties.Resources.delete;
            this.deleteServerButton.Location = new System.Drawing.Point(225, 500);
            this.deleteServerButton.Name = "deleteServerButton";
            this.deleteServerButton.Size = new System.Drawing.Size(45, 35);
            this.deleteServerButton.TabIndex = 6;
            this.deleteServerButton.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.toolTipButtons.SetToolTip(this.deleteServerButton, "Delete Server");
            this.deleteServerButton.UseVisualStyleBackColor = true;
            this.deleteServerButton.Click += new System.EventHandler(this.deleteServerButton_Click);
            // 
            // editServerButton
            // 
            this.editServerButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.editServerButton.Enabled = false;
            this.editServerButton.Image = global::ESExplorer.Properties.Resources.pencil;
            this.editServerButton.Location = new System.Drawing.Point(51, 500);
            this.editServerButton.Name = "editServerButton";
            this.editServerButton.Size = new System.Drawing.Size(45, 35);
            this.editServerButton.TabIndex = 5;
            this.editServerButton.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.toolTipButtons.SetToolTip(this.editServerButton, "Edit Server");
            this.editServerButton.UseVisualStyleBackColor = true;
            this.editServerButton.Click += new System.EventHandler(this.editServerButton_Click);
            // 
            // addServerButton
            // 
            this.addServerButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.addServerButton.Image = global::ESExplorer.Properties.Resources.plus;
            this.addServerButton.Location = new System.Drawing.Point(0, 500);
            this.addServerButton.Name = "addServerButton";
            this.addServerButton.Size = new System.Drawing.Size(45, 35);
            this.addServerButton.TabIndex = 4;
            this.toolTipButtons.SetToolTip(this.addServerButton, "Add Server");
            this.addServerButton.UseVisualStyleBackColor = true;
            this.addServerButton.Click += new System.EventHandler(this.addServerButton_Click);
            // 
            // treeView
            // 
            this.treeView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.treeView.BackColor = System.Drawing.Color.Black;
            this.treeView.ContextMenuStrip = this.serverContextMenuStrip;
            this.treeView.ForeColor = System.Drawing.Color.White;
            this.treeView.HideSelection = false;
            this.treeView.Location = new System.Drawing.Point(0, 0);
            this.treeView.Margin = new System.Windows.Forms.Padding(0, 3, 0, 0);
            this.treeView.Name = "treeView";
            this.treeView.Size = new System.Drawing.Size(270, 497);
            this.treeView.TabIndex = 0;
            // 
            // serverContextMenuStrip
            // 
            this.serverContextMenuStrip.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.serverContextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.foreachDocumentToolStripMenuItem});
            this.serverContextMenuStrip.Name = "serverContextMenuStrip";
            this.serverContextMenuStrip.Size = new System.Drawing.Size(210, 30);
            // 
            // foreachDocumentToolStripMenuItem
            // 
            this.foreachDocumentToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.deleteFieldToolStripMenuItem});
            this.foreachDocumentToolStripMenuItem.Name = "foreachDocumentToolStripMenuItem";
            this.foreachDocumentToolStripMenuItem.Size = new System.Drawing.Size(209, 26);
            this.foreachDocumentToolStripMenuItem.Text = "Foreach Document";
            // 
            // deleteFieldToolStripMenuItem
            // 
            this.deleteFieldToolStripMenuItem.Name = "deleteFieldToolStripMenuItem";
            this.deleteFieldToolStripMenuItem.Size = new System.Drawing.Size(164, 26);
            this.deleteFieldToolStripMenuItem.Text = "Delete Field";
            // 
            // splitContainer1
            // 
            this.splitContainer1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainer1.Location = new System.Drawing.Point(0, 60);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.BackColor = System.Drawing.SystemColors.Control;
            this.splitContainer1.Panel1.Controls.Add(this.queryGroupBox);
            this.splitContainer1.Panel1MinSize = 200;
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.resultGroupBox);
            this.splitContainer1.Panel2MinSize = 200;
            this.splitContainer1.Size = new System.Drawing.Size(808, 475);
            this.splitContainer1.SplitterDistance = 400;
            this.splitContainer1.SplitterWidth = 10;
            this.splitContainer1.TabIndex = 1;
            // 
            // queryGroupBox
            // 
            this.queryGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.queryGroupBox.Controls.Add(this.queryTextBox);
            this.queryGroupBox.Controls.Add(this.verifyAndFormatButton);
            this.queryGroupBox.Controls.Add(this.runButton);
            this.queryGroupBox.Location = new System.Drawing.Point(0, 0);
            this.queryGroupBox.Margin = new System.Windows.Forms.Padding(0, 0, 5, 0);
            this.queryGroupBox.Name = "queryGroupBox";
            this.queryGroupBox.Size = new System.Drawing.Size(395, 472);
            this.queryGroupBox.TabIndex = 7;
            this.queryGroupBox.TabStop = false;
            this.queryGroupBox.Text = "Query";
            // 
            // queryTextBox
            // 
            this.queryTextBox.AcceptsReturn = true;
            this.queryTextBox.AcceptsTab = true;
            this.queryTextBox.AllowDrop = true;
            this.queryTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.queryTextBox.BackColor = System.Drawing.Color.Black;
            this.queryTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.queryTextBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.queryTextBox.Font = new System.Drawing.Font("Lucida Console", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.queryTextBox.ForeColor = System.Drawing.Color.White;
            this.queryTextBox.Location = new System.Drawing.Point(9, 21);
            this.queryTextBox.Multiline = true;
            this.queryTextBox.Name = "queryTextBox";
            this.queryTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.queryTextBox.Size = new System.Drawing.Size(380, 404);
            this.queryTextBox.TabIndex = 8;
            this.queryTextBox.Text = "{\r\n    \"query\": {\r\n        \"match_all\": {}\r\n    }\r\n}";
            this.queryTextBox.WordWrap = false;
            this.queryTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.queryTextBox_KeyPress);
            // 
            // verifyAndFormatButton
            // 
            this.verifyAndFormatButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.verifyAndFormatButton.Image = global::ESExplorer.Properties.Resources.tick;
            this.verifyAndFormatButton.Location = new System.Drawing.Point(9, 431);
            this.verifyAndFormatButton.Name = "verifyAndFormatButton";
            this.verifyAndFormatButton.Size = new System.Drawing.Size(45, 35);
            this.verifyAndFormatButton.TabIndex = 2;
            this.toolTipButtons.SetToolTip(this.verifyAndFormatButton, "Format & Validate");
            this.verifyAndFormatButton.UseVisualStyleBackColor = true;
            this.verifyAndFormatButton.Click += new System.EventHandler(this.verifyAndFormatButton_Click);
            // 
            // apiMethodComboBox
            // 
            this.apiMethodComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.apiMethodComboBox.BackColor = System.Drawing.Color.Black;
            this.apiMethodComboBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.apiMethodComboBox.Font = new System.Drawing.Font("Lucida Console", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.apiMethodComboBox.ForeColor = System.Drawing.Color.White;
            this.apiMethodComboBox.FormattingEnabled = true;
            this.apiMethodComboBox.Items.AddRange(new object[] {
            "_count",
            "_explain",
            "_msearch",
            "_search",
            "_search/exists",
            "_search_shards",
            "_validate/query"});
            this.apiMethodComboBox.Location = new System.Drawing.Point(665, 21);
            this.apiMethodComboBox.Name = "apiMethodComboBox";
            this.apiMethodComboBox.Size = new System.Drawing.Size(123, 21);
            this.apiMethodComboBox.Sorted = true;
            this.apiMethodComboBox.TabIndex = 6;
            this.apiMethodComboBox.Text = "_search";
            // 
            // resultGroupBox
            // 
            this.resultGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.resultGroupBox.Controls.Add(this.resultTextBox);
            this.resultGroupBox.Location = new System.Drawing.Point(5, 0);
            this.resultGroupBox.Margin = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.resultGroupBox.Name = "resultGroupBox";
            this.resultGroupBox.Size = new System.Drawing.Size(393, 472);
            this.resultGroupBox.TabIndex = 12;
            this.resultGroupBox.TabStop = false;
            this.resultGroupBox.Text = "Result";
            // 
            // resultTextBox
            // 
            this.resultTextBox.AcceptsReturn = true;
            this.resultTextBox.AcceptsTab = true;
            this.resultTextBox.AllowDrop = true;
            this.resultTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.resultTextBox.BackColor = System.Drawing.Color.Black;
            this.resultTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.resultTextBox.Font = new System.Drawing.Font("Lucida Console", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.resultTextBox.ForeColor = System.Drawing.Color.White;
            this.resultTextBox.Location = new System.Drawing.Point(6, 21);
            this.resultTextBox.Multiline = true;
            this.resultTextBox.Name = "resultTextBox";
            this.resultTextBox.ReadOnly = true;
            this.resultTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.resultTextBox.Size = new System.Drawing.Size(381, 445);
            this.resultTextBox.TabIndex = 2;
            this.resultTextBox.WordWrap = false;
            // 
            // endpointGroupBox
            // 
            this.endpointGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.endpointGroupBox.Controls.Add(this.apiEndpointTextBox);
            this.endpointGroupBox.Controls.Add(this.methodComboBox);
            this.endpointGroupBox.Controls.Add(this.apiMethodComboBox);
            this.endpointGroupBox.Location = new System.Drawing.Point(0, 0);
            this.endpointGroupBox.Margin = new System.Windows.Forms.Padding(0);
            this.endpointGroupBox.Name = "endpointGroupBox";
            this.endpointGroupBox.Size = new System.Drawing.Size(808, 57);
            this.endpointGroupBox.TabIndex = 2;
            this.endpointGroupBox.TabStop = false;
            this.endpointGroupBox.Text = "Endpoint";
            // 
            // methodComboBox
            // 
            this.methodComboBox.BackColor = System.Drawing.Color.Black;
            this.methodComboBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.methodComboBox.Font = new System.Drawing.Font("Lucida Console", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.methodComboBox.ForeColor = System.Drawing.Color.White;
            this.methodComboBox.FormattingEnabled = true;
            this.methodComboBox.Items.AddRange(new object[] {
            "DELETE",
            "GET",
            "POST",
            "PUT"});
            this.methodComboBox.Location = new System.Drawing.Point(9, 21);
            this.methodComboBox.Name = "methodComboBox";
            this.methodComboBox.Size = new System.Drawing.Size(103, 21);
            this.methodComboBox.Sorted = true;
            this.methodComboBox.TabIndex = 7;
            this.methodComboBox.Text = "GET";
            // 
            // apiEndpointTextBox
            // 
            this.apiEndpointTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.apiEndpointTextBox.BackColor = System.Drawing.Color.Black;
            this.apiEndpointTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.apiEndpointTextBox.Font = new System.Drawing.Font("Lucida Console", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.apiEndpointTextBox.ForeColor = System.Drawing.Color.White;
            this.apiEndpointTextBox.Location = new System.Drawing.Point(118, 21);
            this.apiEndpointTextBox.Name = "apiEndpointTextBox";
            this.apiEndpointTextBox.Size = new System.Drawing.Size(541, 20);
            this.apiEndpointTextBox.TabIndex = 8;
            this.apiEndpointTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // runButton
            // 
            this.runButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.runButton.Image = global::ESExplorer.Properties.Resources.right;
            this.runButton.Location = new System.Drawing.Point(344, 431);
            this.runButton.Name = "runButton";
            this.runButton.Size = new System.Drawing.Size(45, 35);
            this.runButton.TabIndex = 1;
            this.toolTipButtons.SetToolTip(this.runButton, "Run");
            this.runButton.UseVisualStyleBackColor = true;
            this.runButton.Click += new System.EventHandler(this.runButton_Click);
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1112, 577);
            this.Controls.Add(this.mainContainer);
            this.Controls.Add(this.mainToolStrip);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(1130, 320);
            this.Name = "Main";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ES Explorer";
            this.mainToolStrip.ResumeLayout(false);
            this.mainToolStrip.PerformLayout();
            this.mainContainer.Panel1.ResumeLayout(false);
            this.mainContainer.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.mainContainer)).EndInit();
            this.mainContainer.ResumeLayout(false);
            this.serverContextMenuStrip.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.queryGroupBox.ResumeLayout(false);
            this.queryGroupBox.PerformLayout();
            this.resultGroupBox.ResumeLayout(false);
            this.resultGroupBox.PerformLayout();
            this.endpointGroupBox.ResumeLayout(false);
            this.endpointGroupBox.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.ToolStrip mainToolStrip;
        private System.Windows.Forms.ToolStripDropDownButton toolStripDropDownButton1;
        private System.Windows.Forms.ToolStripMenuItem fileExitButton;
        private System.Windows.Forms.ToolStripDropDownButton toolStripDropDownButton2;
        private System.Windows.Forms.ToolStripMenuItem helpAboutButton;
        private System.Windows.Forms.SplitContainer mainContainer;
        private System.Windows.Forms.TreeView treeView;
        private System.Windows.Forms.Button deleteServerButton;
        private System.Windows.Forms.Button editServerButton;
        private System.Windows.Forms.Button addServerButton;
        private System.Windows.Forms.ContextMenuStrip serverContextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem foreachDocumentToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deleteFieldToolStripMenuItem;
        private System.Windows.Forms.ToolTip toolTipButtons;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Button verifyAndFormatButton;
        private System.Windows.Forms.GroupBox resultGroupBox;
        private System.Windows.Forms.GroupBox queryGroupBox;
        private System.Windows.Forms.ComboBox apiMethodComboBox;
        private System.Windows.Forms.TextBox queryTextBox;
        private System.Windows.Forms.TextBox resultTextBox;
        private System.Windows.Forms.GroupBox endpointGroupBox;
        private System.Windows.Forms.TextBox apiEndpointTextBox;
        private System.Windows.Forms.ComboBox methodComboBox;
        private System.Windows.Forms.Button runButton;
    }
}

