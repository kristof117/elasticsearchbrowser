﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.IO;
using System.Collections.Specialized;

namespace ESExplorer
{
    class Http
    {
        protected static WebClient webClient = new WebClient();

        public static String Post(String url, String data)
        {
            return Http.webClient.UploadString(url, data);
        }

        public static String Get(String url)
        {
            return Http.webClient.DownloadString(url);
        }
    }
}
