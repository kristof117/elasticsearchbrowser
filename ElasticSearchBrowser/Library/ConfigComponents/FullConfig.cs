﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace ESExplorer.Library.ConfigComponents
{
    public class FullConfig
    {
        public List<Server> Servers { get; set; }
    }
}
