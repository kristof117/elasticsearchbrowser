﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace ESExplorer.Library.ConfigComponents
{
    public class Server
    {
        public String Name { get; set; }
        public String Url { get; set; }
        
        public Server(String name, String url)
        {
            this.Name = name;
            this.Url = url;
        }
    }
}
