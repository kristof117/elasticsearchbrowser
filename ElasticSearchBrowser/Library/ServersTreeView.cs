﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ESExplorer.Library.ServersTreeViewComponents;
using ESExplorer.Library.ElasticSearchComponents.Response;

namespace ESExplorer.Library
{
    class ServersTreeView
    {
        // The displayable tree view
        private TreeView treeView;
        public TreeView TreeView { get; }

        // The root tree node
        private TreeNode rootTreeNode;

        // Events and event handlers
        public delegate void ServerSelectedEventHandler(ConfigComponents.Server server);
        public event ServerSelectedEventHandler ServerSelectedEvent;
        public delegate void ServerUnselectedEventHandler();
        public event ServerUnselectedEventHandler ServerUnselectedEvent;
        public delegate void IndexSelectedEventHandler(ConfigComponents.Server server, IndexInfo indexInfo);
        public event IndexSelectedEventHandler IndexSelectedEvent;

        /// <summary>
        /// Wraps around a tree view
        /// </summary>
        /// <param name="treeView"></param>
        public ServersTreeView(TreeView treeView)
        {
            this.treeView = treeView;
            this.treeView.NodeMouseDoubleClick += TreeView_NodeMouseDoubleClick;
            this.treeView.AfterSelect += TreeView_AfterSelect;
            this.treeView.AfterExpand += TreeView_AfterExpand;

            // Add root node
            TreeNode root = new TreeNode("Servers");
            this.treeView.Nodes.Add(root);
            this.rootTreeNode = root;
        }

        //
        // EVENT HANDLERS
        //

        private void TreeView_NodeMouseDoubleClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            if (e.Node is ServerTreeNode)
            {
                ServerTreeNode targetNode = (ServerTreeNode)e.Node;
                System.Console.WriteLine("double clicked: " + targetNode.Server.Name);
            }
        }
        
        private void TreeView_AfterSelect(object sender, TreeViewEventArgs e)
        {
            if (e.Node is ServerTreeNode)
            {
                if (this.ServerSelectedEvent != null)
                {
                    ServerTreeNode serverNode = (ServerTreeNode)e.Node;
                    ServerSelectedEvent(serverNode.Server);
                }
            }
            else if (e.Node is IndexTreeNode)
            {
                if (this.IndexSelectedEvent != null)
                {
                    IndexTreeNode indexNode = (IndexTreeNode)e.Node;
                    ServerTreeNode serverNode = (ServerTreeNode)indexNode.Parent;
                    IndexSelectedEvent(serverNode.Server, indexNode.IndexInfo);
                }
            }
            else
            {
                if (this.ServerUnselectedEvent != null)
                {
                    ServerUnselectedEvent();
                }
            }
        }
        
        private void TreeView_AfterExpand(object sender, TreeViewEventArgs e)
        {
            // If it's a Server node, and the indices haven't been loaded yet
            if (e.Node is ServerTreeNode && e.Node.Nodes[0] is LoadingTreeNode)
            {
                ServerTreeNode serverTreeNode = (ServerTreeNode)e.Node;
                
                // CLear nodes
                serverTreeNode.Nodes.Clear();

                // Get indices
                try
                {
                    ElasticSearch es = new ElasticSearch(serverTreeNode.Server.Url);
                    List<IndexInfo> indices = es.GetIndices();
                    
                    // Add them as nodes
                    if (indices.Count > 0)
                    {
                        foreach (IndexInfo indexInfo in indices)
                        {
                            serverTreeNode.Nodes.Add(
                                new IndexTreeNode(indexInfo)
                            );
                        }
                    }
                    // No indices?
                    else
                    {
                        serverTreeNode.Nodes.Add(
                            new NoIndicesTreeNode()
                        );
                    }
                    
                }
                catch (CannotConnectToHostException)
                {
                    serverTreeNode.Nodes.Add(
                        new ErrorTreeNode("Error: Can't connect to host")
                    );
                }
                catch (MalformedResponseException)
                {
                    serverTreeNode.Nodes.Add(
                        new ErrorTreeNode("Error: Malformed response")
                    );
                }
                catch (Exception)
                {
                    serverTreeNode.Nodes.Add(
                        new ErrorTreeNode("Error: Unexpected error")
                    );
                }
            }
        }

        /// <summary>
        /// Creates the nodes based on the config
        /// </summary>
        /// <param name="servers">A List of Server objects</param>
        public void LoadServers(List<ConfigComponents.Server> servers)
        {
            this.rootTreeNode.Nodes.Clear();

            // Add servers to root
            foreach (ConfigComponents.Server server in servers)
            {
                this.AddServer(server);
            }

            this.rootTreeNode.Expand();
        }

        /// <summary>
        /// Adds a server node
        /// </summary>
        /// <param name="server">A Server object</param>
        public void AddServer(ConfigComponents.Server server)
        {
            // Create server node
            ServerTreeNode node = new ServerTreeNode(server);

            // Create loading sub-node
            LoadingTreeNode loading = new LoadingTreeNode();
            node.Nodes.Add(loading);

            // Add to root
            this.rootTreeNode.Nodes.Add(node);
        }

        /// <summary>
        /// Removes a server node
        /// </summary>
        /// <param name="server">A Server object</param>
        public void RemoveServer(ConfigComponents.Server server)
        {
            foreach (ServerTreeNode node in this.rootTreeNode.Nodes)
            {
                if (node.Server == server)
                {
                    this.rootTreeNode.Nodes.Remove(node);
                    break;
                }
            }
        }

        /// <summary>
        /// Remove a server node    
        /// </summary>
        /// <param name="original">The server with the original values</param>
        /// <param name="edited">The server with the new values</param>
        public void EditServer(ConfigComponents.Server original, ConfigComponents.Server edited)
        {
            foreach (ServerTreeNode node in this.rootTreeNode.Nodes)
            {
                if (node.Server == original)
                {
                    node.Text = edited.Name;
                    node.Server.Name = edited.Name;
                    node.Server.Url = edited.Url;
                    break;
                }
            }
        }
    }
}
