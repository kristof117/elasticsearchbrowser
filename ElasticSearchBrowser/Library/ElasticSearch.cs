﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using ESExplorer.Library.ElasticSearchComponents.Response;

namespace ESExplorer.Library
{
    class ElasticSearch
    {
        protected String url;
        
        public ElasticSearch(String url)
        {
            url = url.TrimEnd(new char[] { ' ', '/' });

            // Not valid format?
            if (!FormValidation.ValidUrl(url))
            {
                throw new InvalidUrlFormatException();
            }

            this.url = url;
        }

        public ServerInfo GetServerInfo()
        {
            try
            {
                String response = Http.Get(this.url);
                return JsonConvert.DeserializeObject<ServerInfo>(response);
            }
            catch (System.Net.WebException)
            {
                throw new CannotConnectToHostException();
            }
            catch (Exception)
            {
                throw new MalformedResponseException();
            }
        }

        public List<IndexInfo> GetIndices()
        {
            try
            {
                String response = Http.Get(this.url + "/_cat/indices?format=json");
                return JsonConvert.DeserializeObject<List<IndexInfo>>(response);
            }
            catch (System.Net.WebException)
            {
                throw new CannotConnectToHostException();
            }
            catch (Exception)
            {
                throw new MalformedResponseException();
            }
        }
    }

    class InvalidUrlFormatException : Exception
    {
        public InvalidUrlFormatException() : base() { }
        public InvalidUrlFormatException(String message, Exception previous) : base(message, previous) { }
    }

    class CannotConnectToHostException : Exception
    {
        public CannotConnectToHostException() : base() { }
    }

    class MalformedResponseException : Exception
    {
        public MalformedResponseException() : base() { }
    }
}
