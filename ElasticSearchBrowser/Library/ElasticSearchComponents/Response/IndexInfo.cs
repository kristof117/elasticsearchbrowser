﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace ESExplorer.Library.ElasticSearchComponents.Response
{
    [DataContract]
    public class IndexInfo
    {
        [DataMember(Name = "health")]
        public String Health { get; set; }

        [DataMember(Name = "status")]
        public String Status { get; set; }

        [DataMember(Name = "index")]
        public String Index { get; set; }

        [DataMember(Name = "pri")]
        public String Pri { get; set; }

        [DataMember(Name = "rep")]
        public String Rep { get; set; }

        [DataMember(Name = "docs.count")]
        public String DocsCount { get; set; }

        [DataMember(Name = "docs.deleted")]
        public String DocsDeleted { get; set; }

        [DataMember(Name = "store.size")]
        public String StoreSize { get; set; }

        [DataMember(Name = "pri.store.size")]
        public String PriStoreSize { get; set; }
    }
}
