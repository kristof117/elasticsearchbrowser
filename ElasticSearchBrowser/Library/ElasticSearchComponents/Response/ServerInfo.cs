﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace ESExplorer.Library.ElasticSearchComponents.Response
{
    [DataContract]
    public class ServerInfo
    {
        [DataMember(Name = "status")]
        public int Status { get; set; }

        [DataMember(Name = "name")]
        public String Name { get; set; }

        [DataMember(Name = "cluster_name")]
        public String ClusterName { get; set; }

        [DataMember(Name = "version")]
        public Version Version { get; set; }

        [DataMember(Name = "tagline")]
        public String Tagline { get; set; }
    }

    [DataContract]
    public class Version
    {
        [DataMember(Name = "number")]
        public String Number { get; set; }

        [DataMember(Name = "build_hash")]
        public String BuildHash { get; set; }

        [DataMember(Name = "build_timestamp")]
        public String BuildTimestamp { get; set; }

        [DataMember(Name = "build_snapshot")]
        public bool BuildSnapshot { get; set; }

        [DataMember(Name = "lucene_version")]
        public String LuceneVersion { get; set; }
    }
}
