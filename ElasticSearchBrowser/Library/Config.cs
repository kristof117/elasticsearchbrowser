﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Reflection;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace ESExplorer.Library
{
    public class Config
    {
        // Once the constructor receives the filename, the full filepath is stored here
        private String configFileNameWithPath;

        // The config
        public ConfigComponents.FullConfig FullConfig { get; set; }

        /// <summary>
        /// Loads the contents of the config file
        /// </summary>
        /// <param name="configFileName">Filename relative to project root</param>
        public Config(String configFileName)
        {
            // Assemble config file name with path
            String projectRoot = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            this.configFileNameWithPath = projectRoot + Path.DirectorySeparatorChar + configFileName;

            // Load
            this.Reload();
        }

        /// <summary>
        /// Add a server and save it
        /// </summary>
        /// <param name="server"></param>
        public void AddServer(ConfigComponents.Server server)
        {
            this.FullConfig.Servers.Add(server);
            this.SaveAndReload();
        }

        /// <summary>
        /// Remove a server and save it
        /// </summary>
        /// <param name="server"></param>
        public void RemoveServer(ConfigComponents.Server server)
        {
            this.FullConfig.Servers.Remove(server);
            this.SaveAndReload();
        }

        /// <summary>
        /// Edit a server and save it
        /// </summary>
        /// <param name="original">The server with the original values</param>
        /// <param name="edited">The server with the new values</param>
        public void EditServer(ConfigComponents.Server original, ConfigComponents.Server edited)
        {
            // Find and update the details
            ConfigComponents.Server found = this.FullConfig.Servers.Find(aServer => {
                return aServer.Name == original.Name && aServer.Url == original.Url;
            });
            found.Name = edited.Name;
            found.Url = edited.Url;

            // Save
            this.SaveAndReload();
        }

        /// <summary>
        /// Reloads the config
        /// </summary>
        public void Reload()
        {
            String fileContents = File.ReadAllText(this.configFileNameWithPath);
            this.FullConfig = JsonConvert.DeserializeObject<ConfigComponents.FullConfig>(fileContents);
        }

        /// <summary>
        /// Save the config into the file
        /// </summary>
        private void Save()
        {
            String newFileContents = JsonConvert.SerializeObject(
                this.FullConfig,
                Formatting.Indented
            );
            File.WriteAllText(this.configFileNameWithPath, newFileContents);
        }

        /// <summary>
        /// Save the config into the file and reload
        /// </summary>
        private void SaveAndReload()
        {
            this.Save();
            this.Reload();
        }
    }
}
