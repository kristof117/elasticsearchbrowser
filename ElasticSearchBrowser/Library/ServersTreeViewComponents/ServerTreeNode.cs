﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ESExplorer.Library.ConfigComponents;

namespace ESExplorer.Library.ServersTreeViewComponents
{
    class ServerTreeNode : TreeNode
    {
        // A Server is tied to a TreeNode
        private Server server;
        public Server Server { get { return this.server; } }
        
        /// <summary>
        /// Creates the base TreeNode and store the Server
        /// </summary>
        /// <param name="server"></param>
        public ServerTreeNode(Server server) : base(server.Name)
        {
            this.server = server;
        }
    }
}
