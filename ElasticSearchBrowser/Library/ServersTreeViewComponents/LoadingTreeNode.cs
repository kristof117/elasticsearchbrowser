﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ESExplorer.Library.ConfigComponents;

namespace ESExplorer.Library.ServersTreeViewComponents
{
    class LoadingTreeNode : TreeNode
    {
        public LoadingTreeNode() : base("Loading...") { }
    }
}
