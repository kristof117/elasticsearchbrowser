﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ESExplorer.Library.ElasticSearchComponents.Response;

namespace ESExplorer.Library.ServersTreeViewComponents
{
    class IndexTreeNode : TreeNode
    {
        // An IndexInfo is tied to a TreeNode
        private IndexInfo indexInfo;
        public IndexInfo IndexInfo { get { return this.indexInfo; } }
        
        /// <summary>
        /// Creates the base TreeNode and stores the IndexInfo
        /// </summary>
        /// <param name="indexInfo"></param>
        public IndexTreeNode(IndexInfo indexInfo) : base(indexInfo.Index + " (" + indexInfo.DocsCount + ")")
        {
            this.indexInfo = indexInfo;
        }
    }
}
