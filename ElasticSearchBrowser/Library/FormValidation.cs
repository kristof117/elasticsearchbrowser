﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace ESExplorer.Library
{
    class FormValidation
    {
        public static bool ValidUrl(String url)
        {
            Regex regex = new Regex("^http[s]?[:][/][/].+$", RegexOptions.IgnoreCase);
            Match match = regex.Match(url);
            return match.Success;
        }
    }
}
