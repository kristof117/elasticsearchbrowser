﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Collections.Specialized;
using ESExplorer.Library;
using ESExplorer.Library.ConfigComponents;
using ESExplorer.Library.ElasticSearchComponents.Response;
using JsonPrettyPrinterPlus;
using JsonPrettyPrinterPlus.JsonPrettyPrinterInternals;

namespace ESExplorer
{
    public partial class Main : Form
    {
        private String defaultApiEndpoint = "(Select a server/index...)";
        private JsonPrettyPrinter jsonPrettyPrinter;

        private Config config;
        private ServersTreeView serversTreeView;
        private Server selectedServer;
        private IndexInfo selectedIndex;

        private About aboutForm;
        private AddEditServer addServerForm;

        public Main()
        {
            InitializeComponent();

            this.apiEndpointTextBox.Text = this.defaultApiEndpoint;
            this.config = new Config("config.json");

            this.serversTreeView = new ServersTreeView(this.treeView);
            this.serversTreeView.LoadServers(this.config.FullConfig.Servers);
            this.serversTreeView.ServerSelectedEvent += new ServersTreeView.ServerSelectedEventHandler(ServerSelected);
            this.serversTreeView.ServerUnselectedEvent += new ServersTreeView.ServerUnselectedEventHandler(ServerUnselected);
            this.serversTreeView.IndexSelectedEvent += new ServersTreeView.IndexSelectedEventHandler(IndexSelected);

            this.jsonPrettyPrinter = new JsonPrettyPrinter(new JsonPPStrategyContext());
        }

        // 
        // TOOLSTRIP ACTIONS
        //

        private void fileExitButton_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void helpAboutButton_Click(object sender, EventArgs e)
        {
            this.aboutForm = new About();
            this.aboutForm.ShowDialog();
        }

        //
        // SERVERS TREE VIEW PANEL
        //

        private void addServerButton_Click(object sender, EventArgs e)
        {
            this.addServerForm = new AddEditServer();
            this.addServerForm.ServerAddedEvent += new AddEditServer.ServerAddedEventHandler(this.config.AddServer);
            this.addServerForm.ServerAddedEvent += new AddEditServer.ServerAddedEventHandler(this.serversTreeView.AddServer);
            this.addServerForm.ShowDialog();
        }

        private void editServerButton_Click(object sender, EventArgs e)
        {
            this.addServerForm = new AddEditServer(this.selectedServer);
            this.addServerForm.ServerEditedEvent += new AddEditServer.ServerEditedEventHandler(this.config.EditServer);
            this.addServerForm.ServerEditedEvent += new AddEditServer.ServerEditedEventHandler(this.serversTreeView.EditServer);
            this.addServerForm.ShowDialog();
        }

        private void deleteServerButton_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show(
                "Are you sure you want delete this server?",
                "Delete Server",
                MessageBoxButtons.YesNoCancel,
                MessageBoxIcon.Question,
                MessageBoxDefaultButton.Button2
            );
            
            if (result == DialogResult.Yes)
            {
                this.config.RemoveServer(this.selectedServer);
                this.serversTreeView.RemoveServer(this.selectedServer);
            }
        }

        //
        // EVENT LISTENERS
        //

        public void ServerSelected(Server server)
        {
            this.editServerButton.Enabled = true;
            this.deleteServerButton.Enabled = true;

            this.selectedServer = server;
            this.apiEndpointTextBox.Text = server.Url + "/*/";
            this.apiEndpointTextBox.SelectionStart = this.apiEndpointTextBox.Text.Length - 1;
        }

        public void ServerUnselected()
        {
            this.editServerButton.Enabled = false;
            this.deleteServerButton.Enabled = false;

            this.apiEndpointTextBox.Text = this.defaultApiEndpoint;
        }

        public void IndexSelected(Server server, IndexInfo indexInfo)
        {
            this.editServerButton.Enabled = true;
            this.deleteServerButton.Enabled = true;

            this.selectedServer = server;
            this.selectedIndex = indexInfo;
            this.apiEndpointTextBox.Text = server.Url + "/" + indexInfo.Index;
            this.apiEndpointTextBox.SelectionStart = this.apiEndpointTextBox.Text.Length - 1;
        }

        //
        // TEXTBOX OVERRIDES
        //

        private void queryTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Tab)
            {
                String tabReplacement = "    ";

                e.Handled = true;

                int position = this.queryTextBox.SelectionStart;
                this.queryTextBox.Text = this.queryTextBox.Text.Insert(position, tabReplacement);
                this.queryTextBox.Select(position + tabReplacement.Length, 0);
            }
        }

        //
        // QUERY
        //
        
        private void runButton_Click(object sender, EventArgs e)
        {
            if (this.methodComboBox.Text == "GET")
            {
                this.resultTextBox.Text = this.jsonPrettyPrinter.PrettyPrint(
                    Http.Get(this.apiEndpointTextBox.Text + "/" + this.apiMethodComboBox.Text)
                );
            }
            else if (this.methodComboBox.Text == "POST")
            {
                this.resultTextBox.Text = this.jsonPrettyPrinter.PrettyPrint(
                    Http.Post(
                        this.apiEndpointTextBox.Text + "/" + this.apiMethodComboBox.Text,
                        this.queryTextBox.Text
                    )
                );
            }
        }

        private void verifyAndFormatButton_Click(object sender, EventArgs e)
        {
            if (this.queryTextBox.Text.Length > 0)
            {
                this.queryTextBox.Text = this.jsonPrettyPrinter.PrettyPrint(this.queryTextBox.Text);
            }
        }
    }
}
