﻿using System;
using System.Windows.Forms;
using ESExplorer.Library.ConfigComponents;
using ESExplorer.Library;

namespace ESExplorer
{
    public partial class AddEditServer : Form
    {
        // If in edit mode: the Server to edit
        private Server serverToEdit;

        // Constants
        private const String defaultUrl = "http://example.com:9200";

        // Events and event handlers
        public delegate void ServerAddedEventHandler(Server server);
        public event ServerAddedEventHandler ServerAddedEvent;
        public delegate void ServerEditedEventHandler(Server original, Server edited);
        public event ServerEditedEventHandler ServerEditedEvent;

        /// <summary>
        /// Add Server
        /// </summary>
        public AddEditServer()
        {
            InitializeComponent();

            this.urlTextBox.Text = AddEditServer.defaultUrl;
        }

        /// <summary>
        /// Edit Server
        /// </summary>
        /// <param name="server"></param>
        public AddEditServer(Server server)
        {
            InitializeComponent();

            this.serverToEdit = server;
            this.Text = "Edit Server";

            this.displayNameTextBox.Text = server.Name;
            this.urlTextBox.Text = server.Url;
        }

        //
        // KEY DOWN
        //

        private void AddEditServer_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Escape)
            {
                this.cancelButton_Click(null, EventArgs.Empty);
            }
            else if (e.KeyChar == (char)Keys.Enter)
            {
                this.saveButton_Click(null, EventArgs.Empty);
            }
        }

        //
        // BUTTONS
        //

        private void cancelButton_Click(object sender, EventArgs e)
        {
            // Any changes have been made to the form fields?
            if ((this.serverToEdit == null && (
                    this.displayNameTextBox.Text.Length != 0 || (
                        this.urlTextBox.Text.Length != 0 && 
                        this.urlTextBox.Text != AddEditServer.defaultUrl
                    )
                )) || (
                    this.serverToEdit != null && (
                        this.displayNameTextBox.Text != this.serverToEdit.Name ||
                        this.urlTextBox.Text != this.serverToEdit.Url
                    )
                ))
            {
                // Ask to discard them
                DialogResult result = MessageBox.Show(
                    "Are you sure you want discard your changes?", "Add Server",
                    MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2
                );

                // If it's not yes, don't do anything
                if (result != DialogResult.Yes)
                {
                    return;
                }
            }

            // Close form
            this.Close();
            this.Dispose();
        }

        private void testButton_Click(object sender, EventArgs e)
        {
            // URL valid?
            if (this.testUrl())
            {
                MessageBox.Show(
                    "Connection successful!", "Test Connection",
                    MessageBoxButtons.OK, MessageBoxIcon.Information
                );
            }
        }

        private void saveButton_Click(object sender, EventArgs e)
        {
            // Display name empty?
            if (this.displayNameTextBox.Text.Length == 0)
            {
                MessageBox.Show(
                    "The display name cannot be empty.", "Add Server",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning
                );
                return;
            }

            // URL valid?
            if (this.testUrl())
            {
                // Save
                if (this.ServerAddedEvent != null)
                {
                    this.ServerAddedEvent(
                        new Server(
                            this.displayNameTextBox.Text,
                            this.urlTextBox.Text
                        )
                    );
                }
                if (this.ServerEditedEvent != null)
                {
                    this.ServerEditedEvent(
                        this.serverToEdit,
                        new Server(
                            this.displayNameTextBox.Text,
                            this.urlTextBox.Text
                        )
                    );
                }

                // Close
                this.Close();
                this.Dispose();
            }
        }
        
        //
        // HELPER FUNCTIONS
        //

        private bool testUrl()
        {
            // Url not valid?
            ElasticSearch testES;
            try
            {
                testES = new ElasticSearch(this.urlTextBox.Text);
            }
            catch (InvalidUrlFormatException)
            {
                MessageBox.Show(
                    "The URL you have entered is not valid.", "Test Connection",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning
                );
                return false;
            }
            catch (Exception)
            {
                MessageBox.Show(
                    "An unexpected error occured why trying to test the URL.", "Test Connection",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning
                );
                return false;
            }

            // Can't connect?
            try
            {
                if (testES.GetServerInfo() == null)
                {
                    MessageBox.Show(
                        "An unexpected error occured while trying to connect.", "Test Connection",
                        MessageBoxButtons.OK, MessageBoxIcon.Warning
                    );
                    return false;
                }
            }
            catch (CannotConnectToHostException)
            {
                MessageBox.Show(
                    "Cannot connect to host.", "Test Connection",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning
                );
                return false;
            }
            catch (MalformedResponseException)
            {
                MessageBox.Show(
                    "Connected to host, but a malformed response was received.", "Test Connection",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning
                );
                return false;
            }

            return true;
        }
    }
}
